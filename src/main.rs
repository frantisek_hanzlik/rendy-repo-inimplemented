use rendy::{
	command::Families,
	factory::Factory,
	graph::{present::PresentNode, Graph, GraphBuilder},
	hal::{
		command::{ClearColor, ClearValue},
		Backend,
	},
	init::{
		winit::{
			event::{Event, WindowEvent},
			event_loop::{ControlFlow, EventLoop, EventLoopWindowTarget},
			window::{Window, WindowBuilder},
		},
		WindowedRendy,
	},
	wsi::Surface,
};

type CurrentBackend = rendy::vulkan::Backend;

#[cfg(any(feature = "dx12", feature = "metal", feature = "vulkan"))]
fn main() {
	let event_loop = EventLoop::new();
	let window_builder = WindowBuilder::new();

	let WindowedRendy {
		mut factory,
		mut families,
		surface,
		window,
	} = init_rendy(window_builder, &event_loop);

	let mut graph = init_graph(&mut factory, &mut families, &window, surface);

	event_loop.run(move |event, window_target, control_flow| {
		handle_event(
			event,
			window_target,
			control_flow,
			&mut factory,
			&mut families,
			&mut graph,
		)
	});
}

fn init_rendy(
	window_builder: WindowBuilder,
	event_loop: &EventLoop<()>,
) -> WindowedRendy<CurrentBackend> {
	WindowedRendy::init(
		&<rendy::factory::Config as Default>::default(),
		window_builder,
		&event_loop,
	)
	.unwrap()
}

fn init_graph<B: Backend>(
	factory: &mut Factory<B>,
	families: &mut Families<B>,
	window: &Window,
	surface: Surface<B>,
) -> Graph<B, ()> {
	let mut graph_builder = GraphBuilder::new();

	let color = graph_builder.create_image(
		window_resource_kind(window),
		1,
		factory.get_surface_format(&surface),
		Some(ClearValue {
			color: ClearColor {
				float32: [1.0, 1.0, 1.0, 1.0],
			},
		}),
	);

	graph_builder.add_node(PresentNode::builder(factory, surface, color));

	graph_builder.build(factory, families, &()).unwrap()
}

fn handle_event<B: Backend>(
	event: Event<()>,
	_: &EventLoopWindowTarget<()>,
	control_flow: &mut ControlFlow,
	factory: &mut Factory<B>,
	families: &mut Families<B>,
	graph: &mut Graph<B, ()>,
) {
	*control_flow = ControlFlow::Poll;

	factory.maintain(families);

	match event {
		Event::WindowEvent {
			event: WindowEvent::CloseRequested,
			..
		} => *control_flow = ControlFlow::Exit,
		_ => {}
	}

	graph.run(factory, families, &());
}

fn window_resource_kind(window: &Window) -> rendy::resource::Kind {
	let size = window.inner_size();

	rendy::resource::Kind::D2(size.width, size.height, 1, 1)
}
